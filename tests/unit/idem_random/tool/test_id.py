import string


def test_generate_random_id(mock_hub, hub):
    mock_hub.tool.random.id.generate_random_id = hub.tool.random.id.generate_random_id

    for i in range(1, 10):
        ret = mock_hub.tool.random.id.generate_random_id(i)
        assert ret[0] in string.ascii_letters
        assert len(ret) == i
        charset = string.ascii_letters + string.digits
        for c in ret:
            assert c in charset
